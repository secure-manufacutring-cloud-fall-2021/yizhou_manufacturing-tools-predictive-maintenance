%%  data extraction 
clear all 
clc
milling_data = load('milling.mat');
%% extract all cases
%% feasibility of incoperating context variables
case1 = milling_data.milling_data.milling_data.milling_data.mill(:,1:15);
case2 = milling_data.milling_data.milling_data.milling_data.mill(:, 16:29);
case3 = milling_data.milling_data.milling_data.milling_data.mill(:, 30:43);
case4 = milling_data.milling_data.milling_data.milling_data.mill(:, 44:50);
case5 = milling_data.milling_data.milling_data.milling_data.mill(:, 108:113);
case6 = milling_data.milling_data.milling_data.milling_data.mill(:, 114);
case7 = milling_data.milling_data.milling_data.milling_data.mill(:, 115:122);
case8 = milling_data.milling_data.milling_data.milling_data.mill(:, 123:128);
case9 = milling_data.milling_data.milling_data.milling_data.mill(:, 51:59);
case10 = milling_data.milling_data.milling_data.milling_data.mill(:, 60:69);
case11 = milling_data.milling_data.milling_data.milling_data.mill(:, 70:92);
case12 = milling_data.milling_data.milling_data.milling_data.mill(:, 93:107);
case13 = milling_data.milling_data.milling_data.milling_data.mill(:, 129:143);
case14 = milling_data.milling_data.milling_data.milling_data.mill(:, 144:152);
case15 = milling_data.milling_data.milling_data.milling_data.mill(:, 153:159);
case16 = milling_data.milling_data.milling_data.milling_data.mill(:, 160:165);
% 
data1 = extractfield(case1,'smcAC');
% data2 = extractfield(case2,'smcAC');
% data3 = extractfield(case3,'smcAC');
% data4 = extractfield(case4,'smcAC');
% data5 = extractfield(case5,'smcAC');
% data6 = extractfield(case6, 'smcAC');
data7 = extractfield(case7,'smcAC');
data9 = extractfield(case9,'smcAC');
% data10 = extractfield(case10,'smcAC');
% data11 = extractfield(case11,'smcAC');
% data12 = extractfield(case12,'smcAC');
data13 = extractfield(case13,'smcAC');
% data14 = extractfield(case14,'smcAC');
% data15 = extractfield(case15,'smcAC');
% data16 = extractfield(case16,'smcAC');

% data1 = extractfield(case1,'vib_spindle');
% data2 = extractfield(case2,'vib_spindle');
% data3 = extractfield(case3,'vib_spindle');
% data4 = extractfield(case4,'vib_spindle');
% data5 = extractfield(case5,'vib_spindle');
% data6 = extractfield(case6, 'vib_spindle');
% data7 = extractfield(case7,'vib_spindle');
% data8 = extractfield(case8,'vib_spindle');
% data9 = extractfield(case9,'vib_spindle');
% data10 = extractfield(case10,'vib_spindle');
% data11 = extractfield(case11,'vib_spindle');
% data12 = extractfield(case12,'vib_spindle');
% data13 = extractfield(case13,'vib_spindle');
% data14 = extractfield(case14,'vib_spindle');
% data15 = extractfield(case15,'vib_spindle');
% data16 = extractfield(case16,'vib_spindle');


% data1 = extractfield(case1,'VB');
% data2 = extractfield(case2,'VB');
% data3 = extractfield(case3,'VB');
% data4 = extractfield(case4,'VB');
% data5 = extractfield(case5,'VB');
% data6 = extractfield(case6, 'VB');
% data7 = extractfield(case7,'VB');
% data8 = extractfield(case8,'VB');
% data9 = extractfield(case9,'VB');
% data10 = extractfield(case10,'VB');
% data11 = extractfield(case11,'VB');
% data12 = extractfield(case12,'VB');
% data13 = extractfield(case13,'VB');
% data14 = extractfield(case14,'VB');
% data15 = extractfield(case15,'VB');
% data16 = extractfield(case16,'VB');

% hold on
% time_data_13 = linspace(0, (length(data13)-1)*0.008, length(data13));
% plot(time_data_13, data13, 'b'), ylabel('smcAC(A)'), xlabel('Operating Times(s)')
% time_data_7 = linspace(0, (length(data7)-1)*0.008, length(data7));
% plot(data1, 'r'), ylabel('smcAC(A)'), xlabel('Operating Time (s)')
% % % subplot(4,4,2), plot(data2), legend('condtion: DOC = 0.75, feed = 0.5, material = 1')
% % % subplot(4,4,3), plot(data3), legend('condtion: DOC = 0.75, feed = 0.25, material = 1')
% % % subplot(4,4,4), plot(data4), legend('condtion: DOC = 1.5, feed = 0.25, material = 1')
% % % subplot(4,4,5), plot(data5), legend('condtion: DOC = 1.5, feed = 0.5, material = 2')
% % % subplot(4,4,6), plot(data6), legend('condtion: DOC = 1.5, feed = 0.25, material = 2')
% % % subplot(4,4,7), plot(data7), legend('condtion: DOC = 0.75, feed = 0.25, material = 2')
% % % subplot(4,4,8), plot(data8), ylabel('VB'), xlabel('Experiement Set'), legend('condtion: DOC = 0.75, feed = 0.5, material = 2')
% 
% legend('case 13: condtion: DOC = 09.75, feed = 0.25, material = 2', 'case 7: condtion: DOC = 0.75, feed = 0.25, material = 2')
% hold off 
% subplot(4,4,10), plot(data10), legend('condtion: DOC = 1.5, feed = 0.25, material = 1')
% subplot(4,4,11), plot(data11), legend('condtion: DOC = 0.75, feed = 0.25, material = 1')
% subplot(4,4,12), plot(data12), legend('condtion: DOC = 0.75, feed = 0.5, material = 1')
% subplot(4,4,13), plot(data13), legend('condtion: DOC = 0.75, feed = 0.25, material = 2')
% subplot(4,4,14), plot(data14), ylabel('VB'), xlabel('Experiement Set'), legend('condtion: DOC = 0.75, feed = 0.5, material = 2')
% subplot(4,4,15), plot(data15), legend('condtion: DOC = 1.5, feed = 0.25, material = 2')
% subplot(4,4,16), plot(data16), legend('condtion: DOC = 1.5, feed = 0.5, material = 2')

mask_data1_P2P = []
for i = 1:length(data9)/9000 
    periodic_data = data9(5000 + (i - 1)*9000:7000 + (i - 1)*9000);
    mean_data = mean(periodic_data)
    selected_data = periodic_data(find(periodic_data > 2));
    selected_data = mean(selected_data)
    mask_data1_P2P(i) = selected_data - mean_data;
end

VB_data1 = extractfield(case1,'VB');
VB_data2 = extractfield(case2,'VB');
VB_data3 = extractfield(case3,'VB');
VB_data4 = extractfield(case4,'VB');
VB_data5 = extractfield(case5,'VB');
VB_data7 = extractfield(case7,'VB');
VB_data8 = extractfield(case8,'VB');
VB_data9 = extractfield(case9,'VB');
VB_data10 = extractfield(case10,'VB');
VB_data11 = extractfield(case11,'VB');
VB_data12 = extractfield(case12,'VB');
VB_data13 = extractfield(case13,'VB');
VB_data14 = extractfield(case14,'VB');
VB_data15 = extractfield(case15,'VB');
VB_data16 = extractfield(case16,'VB');

figure
hold on 
yyaxis left
plot(mask_data1_P2P, 'b')
ylabel('Peak to Peak smcAC(A)')
yyaxis right
plot(VB_data1, 'r')
legend('Peak to Peak smcAC', 'Flank Wear Level')
ylabel('Flank Wear Level (VB)')
xlabel('Experiment Run Times')
title('case 7')
hold off 
% 
% figure
% plot(mask_data1_P2P, VB_data1)

time1 = linspace(1, length(VB_data1), length(VB_data1))';
VB_data1 = VB_data1';
time2 = linspace(2, length(VB_data2), length(VB_data2))';
VB_data2 = VB_data2';
time3 = linspace(1, length(VB_data3), length(VB_data3))';
VB_data3 = VB_data3';
time4 = linspace(1, length(VB_data4), length(VB_data4))';
VB_data4 = VB_data4';
time5 = linspace(1, length(VB_data5), length(VB_data5))';
VB_data5 = VB_data5';
time7 = linspace(1, length(VB_data7), length(VB_data7))';
VB_data7 = VB_data7';
time8 = linspace(1, length(VB_data8), length(VB_data8))';
VB_data8 = VB_data8';
time9 = linspace(1, length(VB_data9), length(VB_data9))';
VB_data9 = VB_data9';
time10 = linspace(1, length(VB_data10), length(VB_data10))';
VB_data10 = VB_data10';
time11 = linspace(1, length(VB_data11), length(VB_data11))';
VB_data11 = VB_data11';
time12 = linspace(1, length(VB_data12), length(VB_data12))';
VB_data12 = VB_data12';
time13 = linspace(1, length(VB_data13), length(VB_data13))';
VB_data13 = VB_data13';
time14 = linspace(1, length(VB_data14), length(VB_data14))';
VB_data14 = VB_data14';
time15 = linspace(1, length(VB_data15), length(VB_data15))';
VB_data15 = VB_data15';
time16 = linspace(1, length(VB_data16), length(VB_data16))';
VB_data16 = VB_data16';