import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


f=open('save_data1.txt',"r")
lines=f.readlines()
data_AC=[]
data_VB=[]
C_T = []
f_r = []
a_p =[]
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(260)
    f_r.append(0.5)
    a_p.append(1.5)
f.close()

f=open('save_data3.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(260)
    f_r.append(0.25)
    a_p.append(0.75)
f.close()

f=open('save_data4.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(260)
    f_r.append(0.25)
    a_p.append(1.5)
f.close()

f=open('save_data5.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(150)
    f_r.append(0.5)
    a_p.append(1.5)
f.close()

f=open('save_data7.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(150)
    f_r.append(0.25)
    a_p.append(0.75)
f.close()
# f=open('save_data8.txt',"r")
# lines=f.readlines()
# for x in lines:
#     data_AC.append(float(x.split(',')[0]))
#     data_VB.append(float(x.split(',')[1]))
#     C_T.append(150)
#     f_r.append(0.5)
#     a_p.append(0.75)
# f.close()

f=open('save_data9.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(260)
    f_r.append(0.5)
    a_p.append(1.5)
f.close()

f=open('save_data10.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(260)
    f_r.append(0.25)
    a_p.append(1.5)
f.close()

# f=open('save_data11.txt',"r")
# lines=f.readlines()
# for x in lines:
#     data_AC.append(float(x.split(',')[0]))
#     data_VB.append(float(x.split(',')[1]))
#     C_T.append(260)
#     f_r.append(0.25)
#     a_p.append(0.75)
# f.close()

f=open('save_data13.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(150)
    f_r.append(0.5)
    a_p.append(0.75)
f.close()

f=open('save_data14.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(150)
    f_r.append(0.5)
    a_p.append(0.75)
f.close()

f=open('save_data15.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(150)
    f_r.append(0.25)
    a_p.append(1.5)
f.close()

f=open('save_data16.txt',"r")
lines=f.readlines()
for x in lines:
    data_AC.append(float(x.split(',')[0]))
    data_VB.append(float(x.split(',')[1]))
    C_T.append(150)
    f_r.append(0.5)
    a_p.append(1.5)
f.close()


def func(X, a, b, c, d, e):
    C_T,f,a_p,AC = X
    return a*pow((AC-e), d)*pow(f, b)*pow(a_p, c)*C_T

# initial guesses for a,b,c:
p0 = (1., 1., 1., -1., 1)
train_data_AC = data_AC[0:86]
train_data_VB = data_VB[0:86]
train_C_T = C_T[0:86]
train_f_r = f_r[0:86]
train_a_p = a_p[0:86]

optimal_parameters, _ = curve_fit(func, (train_C_T, train_f_r, train_a_p, train_data_AC), train_data_VB, p0)
prediction = func((C_T,f_r, a_p, data_AC), optimal_parameters[0], optimal_parameters[1], optimal_parameters[2], optimal_parameters[3],optimal_parameters[4] )
error = abs(prediction - data_VB)
# plt.plot(error)
# plt.show()

# #1
plt.plot(prediction[0:12], '--bo', label='Experiment 1 Model Prdicted VB')
plt.plot(data_VB[0:12], '-r*', label='Experiment 1 Ground Truth VB')
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.legend()
plt.show()
x = np.linspace(0, 8, 9)
print(x)
y = prediction[0:9]
z = np.polyfit(x, y, 2)
def p3(z, x):
    return z[0]*(x**2) + z[1]*x + z[2]

test_x = np.linspace(9, 11, 3)
result = []
for i in test_x:
    predicted = p3(z, i)
    result.append(predicted)

plt.plot(prediction[0:9], '--bo', label='Experiment 1 Model Degradation Label (VB)')
plt.plot(test_x, result, '--go', label='Experiment 1 Model Prdicted Label (VB)')
plt.plot(data_VB[0:12], '-r*', label='Experiment 1 Ground Truth VB')
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.legend()
plt.show()

# # # # #3
plt.plot(prediction[12:25], '--bo', label='Experiment 3 Model Prdicted VB')
plt.plot(data_VB[12:25], '-r*', label='Experiment 3 Ground Truth VB')
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.legend()
plt.show()
# # # # #4
plt.plot(prediction[25:32], '--bo', label='Experiment 4 Model Prdicted VB')
plt.plot(data_VB[25:32], '-r*', label='Experiment 4 Ground Truth VB')
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.legend()
plt.show()
# # # #5
plt.plot(prediction[32:38], '--bo',label='Experiment 5 Model Prdicted VB')
plt.plot(data_VB[32:38], '-r*', label='Experiment 5 Ground Truth VB')
plt.legend()
plt.show()
# # # #7
plt.plot(prediction[38:46], '--bo',label='Experiment 7 Model Prdicted VB')
plt.plot(data_VB[38:46], '-r*', label='Experiment 7 Ground Truth VB')
plt.legend()
plt.show()
# # # # #9
plt.plot(prediction[46:54], '--bo', label='Experiment 9 Model Prdicted VB')
plt.plot(data_VB[46:54], '-r*', label='Experiment 9 Ground Truth VB')
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.legend()
plt.show()
# # # #10
plt.plot(prediction[54:64], '--bo', label='Experiment 10 Model Prdicted VB')
plt.plot(data_VB[54:64], '-r*', label='Experiment 10 Ground Truth VB')
plt.legend()
plt.show()
# # # # 13
plt.plot(prediction[64:77], '--bo', label='Experiment 13 Model Prdicted VB')
plt.plot(data_VB[64:77], '-r*', label='Experiment 13 Ground Truth VB')
plt.legend()
plt.show()
# # # # # 14
plt.plot(prediction[77:86], '--bo', label='Experiment 14 Model Prdicted VB')
plt.plot(data_VB[77:86], '-r*', label='Experiment 14 Ground Truth VB')
plt.legend()
plt.show()
# # # # # # 15
plt.plot(prediction[86:93], '--bo', label='Experiment 15 Model Prdicted VB')
plt.plot(data_VB[86:93], '-r*', label='Experiment 15 Ground Truth VB')
plt.legend()
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.show()
# #16
plt.plot(prediction[93:99], '--bo', label='Experiment 16 Model Prdicted VB')
plt.plot(data_VB[93:99], '-r*', label='Experiment 16 Ground Truth VB')
plt.legend()
plt.xlabel('Experiment Run Times')
plt.ylabel('VB')
plt.show()

# trend prediction
plt.plot(data_AC[0:12], data_VB[0:12], '--bo', label='Experiment 1 Model Prdicted VB')
plt.plot(data_AC[46:54], data_VB[46:54], '-r*', label='Experiment 9 Model Prdicted VB')
plt.xlabel('smcAC P2P(A)')
plt.ylabel('Experimental Tool Wear Level(VB)')
plt.legend()
plt.show()

plt.plot(data_AC[25:32], data_VB[25:32], '--bo', label='Experiment 4 Model Prdicted VB')
plt.plot(data_AC[54:64], data_VB[54:64], '-r*', label='Experiment 10 Model Prdicted VB')
plt.xlabel('smcAC P2P(A)')
plt.ylabel('Experimental Tool Wear Level(VB)')
plt.legend()
plt.show()